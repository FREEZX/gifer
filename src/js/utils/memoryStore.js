export const data = {
  videoFile: null,
  videoUrl: null,
  resultBlob: null,
  resultUrl: null,
  textResultBase64: null,
  textResultBlob: null,
  textResultUrl: null,
  savedContexts: null,
  editParams: null,
};

export const dataURItoBlob = (dataURI) => {
  // convert base64/URLEncoded data component to raw binary data held in a string
  var byteString;
  if (dataURI.split(',')[0].indexOf('base64') >= 0)
      byteString = atob(dataURI.split(',')[1]);
  else
      byteString = unescape(dataURI.split(',')[1]);

  // separate out the mime component
  var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

  // write the bytes of the string to a typed array
  var ia = new Uint8Array(byteString.length);
  for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
  }

  return new Blob([ia], {type:mimeString});
}

export const setVideoFile = file => {
  // Clean up old url, if it exist
  if(data.videoUrl) {
    URL.revokeObjectURL(data.videoUrl);
  }

  data.videoFile = file;
  data.videoUrl = URL.createObjectURL(file);
}

export const setResult = result => {
  // Clean up old url, if it exist
  if(data.resultUrl) {
    URL.revokeObjectURL(data.resultUrl);
  }

  data.resultBlob = dataURItoBlob(result);
  data.resultUrl = URL.createObjectURL(data.resultBlob);
}

export const setTextBase64 = result => {
  data.textResultBase64 = result;
}

export const setTextResult = result => {
  // Clean up old url, if it exist
  if(data.textResultUrl) {
    URL.revokeObjectURL(data.textResultUrl);
  }

  data.textResultBlob = dataURItoBlob(result);
  data.textResultUrl = URL.createObjectURL(data.textResultBlob);
}

export const setSavedContexts = contexts => {
  data.savedContexts = contexts;
}

export const setEditParams = params => {
  data.editParams = params;
}

