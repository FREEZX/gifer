export default {
  dark: '#313131',
  highlight: '#0076ff',
  grayed: '#d0d0d0',
  success: '#46CC45',
  error: '#cc4545',
  warning: '#cfbd47',
  softColor: '#fafafa',
};
