import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';
import GiferNavigator from 'components/GiferNavigator';
import { registerWebPlugin } from '@capacitor/core';
import { FileSharer } from '@byteowls/capacitor-filesharer';

import 'onsenui/css/onsenui.css';
import 'onsenui/css/onsen-css-components.css';
import 'typeface-roboto/index.css';

import ons from 'onsenui';
import { Page, Toolbar, Button } from 'react-onsenui';
import { ThemeProvider } from 'styled-components';

import theme from './consts/theme';

ons.platform.select('ios');

const App = () => {
  useEffect(() => {
    registerWebPlugin(FileSharer);
  }, []);
  return (
    <ThemeProvider theme={theme}>
      <GiferNavigator />
    </ThemeProvider>
  );
};

ReactDOM.render(<App />, document.getElementById('app'));
