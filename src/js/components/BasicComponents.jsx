import React from 'react';
import styled from 'styled-components';
import {
  Page as OnsPage,
  Input as OnsInput,
  Range as OnsRange,
  ProgressCircular as OnsProgressCircular
} from 'react-onsenui';
import Button from './Button';

export const Page = styled(OnsPage)`
  .page__background {
    background-color: #fff;
  }
`;

export const H1 = styled.h1`
  font-family: Roboto, Arial, sans-serif;
  color: ${props => props.theme.dark};
`;

export const H2 = styled.h2`
  font-family: Roboto, Arial, sans-serif;
  color: ${props => props.theme.dark};
`;

export const H3 = styled.h3`
  font-family: Roboto, Arial, sans-serif;
  color: ${props => props.theme.dark};
`;

export const H4 = styled.h4`
  font-family: Roboto, Arial, sans-serif;
  color: ${props => props.theme.dark};
`;

export const P = styled.p`
  font-family: Roboto, Arial, sans-serif;
  color: ${props => props.theme.dark};
`;

export const Input = styled(OnsInput)`
  font-family: Roboto, Arial, sans-serif;
  color: ${props => props.theme.dark};
`;

export const ErrorLabel = styled.label`
  font-family: Roboto, Arial, sans-serif;
  color: ${props => props.theme.error};
`;

export const WarningLabel = styled.label`
  font-family: Roboto, Arial, sans-serif;
  color: ${props => props.theme.warning};
`;

export const Range = styled(OnsRange)``;

export const LoadingWrap = styled.section`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const ImageProgressCircular = styled(OnsProgressCircular)`
  width: 64px;
  height: 64px;
`;

export const PreviewContentWrap = styled.div`
  background: ${props => props.theme.softColor};
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`;

export const PreviewWrap = styled.section`
  box-shadow: rgba(0,0,0,0.3) 0 0 10px;
  width: 100%;
  height: 0;
  padding-bottom: 100%;
  position: relative;
  box-sizing: border-box;
`;

export const TaglineText = styled(P)`
  font-size: 25px;
`;

export const ButtonsWrap = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  box-shadow: rgba(0,0,0,.2) 0 0 10px;
  padding-left: 10px;
  padding-right: 10px;
`;

export const ActionButton = styled(Button)`
  flex-grow: 1;
  margin: 20px 10px !important;
`;
