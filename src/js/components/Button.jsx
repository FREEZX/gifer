import React from 'react';
import styled from 'styled-components';
import {Button as OnsButton} from 'react-onsenui';


const ButtonContainer = styled(OnsButton)`
  height: 46px !important;
  border-radius: 5px !important;
  display: flex !important;
  align-content: space-around;
`;

const ButtonText = styled.div`
  line-height: 38px !important;
  font-size: 16pt !important;
  margin: 0;
  flex-grow: 1;
  font-family: Roboto, Arial, sans-serif;
`

const ButtonIcon = styled.img`
  margin-right: 10px;
`

const Button = ({icon, children, ...rest}) => (
  <ButtonContainer ripple {...rest}>
    <ButtonText>{children}</ButtonText>
    {icon && <ButtonIcon src={icon} />}
  </ButtonContainer>
)


export default Button;
