import React from 'react';
import StartPage from 'pages/StartPage';
import EditPage from 'pages/EditPage';
import ResultPage from 'pages/ResultPage';
import AddTextPage from 'pages/AddTextPage';
import { Navigator } from 'react-onsenui';

const GiferNavigator = () => (
  <Navigator renderPage={(route, navigator) => {
    let page;

    const parts = route.path.split('?');

    switch(parts[0]) {
      case '/start':
        page = <StartPage key={route.path} navigator={navigator} />;
        break;
      case '/edit':
        page = <EditPage key={route.path} navigator={navigator} />;
        break;
      case '/text':
        page = <AddTextPage key={route.path} navigator={navigator} />;
        break;
      case '/result':
        page = <ResultPage key={route.path} navigator={navigator} />;
        break;
    }
		return page;
	}}
	initialRoute={{
    path: '/start'
	}} />
);


export default GiferNavigator;
