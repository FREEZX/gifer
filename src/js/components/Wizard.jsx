import React from 'react';
import styled from 'styled-components';

export const WizardContainer = styled.div`
  height: 50px;
  width: calc(100%-30px);
  padding: 15px;
  position: relative;
  background: #FFF;
  z-index: 1;

  &:before {
    content: " ";
    position: absolute;
    top: 29px;
    left: 40px;
    right: 40px;
    border-bottom: 2px solid ${props => props.theme.grayed};
    z-index: -1;
  }
`;

export const WizardStepCircle = styled.div`
  width: 30px;
  height: 30px;
  text-align: center;
  border-radius: 20px;
  color: #FFFFFF;
  background-color: ${props => {
    let color = props.theme.grayed;
    if (props.complete) {
      color = props.theme.success;
    }
    if (props.active) {
      color = props.theme.highlight;
    }
    return color;
  }};
  font-family: Roboto, Arial, sans-serif;
  font-weight: 500;
  font-size: 20px;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const WizardStepsContainer = styled.div`
  width: 100%;
  display: flexbox;
  justify-content: space-between;
`;

export const WizardStepContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const WizardStepText = styled.div`
  margin-top: 5px;
  text-align: center;
  color: ${props => props.active ? props.theme.highlight : props.theme.grayed};
`;

export const WizardStep = ({step, active, complete, children}) => (
  <WizardStepContainer>
    <WizardStepCircle active={active} complete={complete}>{step}</WizardStepCircle>
    <WizardStepText active={active} complete={complete}>{children}</WizardStepText>
  </WizardStepContainer>
);

export default ({step}) => {
  const stepInt = parseInt(step);
  return (
    <WizardContainer>
      <WizardStepsContainer>
        <WizardStep step="1" active={stepInt == 1} complete={stepInt > 1}>Pick vid</WizardStep>
        <WizardStep step="2" active={stepInt == 2} complete={stepInt > 2}>Clip</WizardStep>
        <WizardStep step="3" active={stepInt == 3} complete={stepInt > 3}>Add text</WizardStep>
        <WizardStep step="4" active={stepInt == 4} complete={stepInt > 4}>Share</WizardStep>
      </WizardStepsContainer>
    </WizardContainer>
  )
};
