import styled from 'styled-components';
import React, { useRef, useEffect, useState } from 'react';

import gifIcon from 'static/images/gif.svg';
import refreshIcon from 'static/images/refresh.svg';

import {
  Row,
  Col,
  Icon,
  Radio,
  ProgressBar,
  BackButton,
  Toolbar,
} from 'react-onsenui';
import {
  Page,
  H1,
  H2,
  H4,
  P,
  Input,
  Range,
  LoadingWrap,
  ImageProgressCircular,
  PreviewWrap,
  PreviewContentWrap,
  ButtonsWrap,
  ActionButton,
} from 'components/BasicComponents';
import Button from 'components/Button';
import Wizard from 'components/Wizard';

import { data, setTextResult, setTextBase64 } from 'utils/memoryStore';
import gifshot from 'gifshot';

const HeadImage = styled.img`
  width: 100%;
  max-height: 100%;
  object-fit: contain;
`;

const GifText = styled.div`
  width: 100%;
  color: #ffffff;
  position: absolute;
  text-align: center;
`;

const PageWrap = styled.section`
  height: 100%;
  display: flex;
  flex-direction: column;
`;

const SettingsWrap = styled.section`
  margin: 0;
  padding: 0 20px;
  display: block;
  justify-content: flex-start;
  flex-grow: 1;
  overflow-y: scroll;
`;

let captureOptions = {};

const AddTextPage = ({ navigator }) => {
  const [aspect, setAspect] = useState(
    data.savedContexts[0].width / data.savedContexts[0].height
  );
  const [processing, setProcessing] = useState(false);

  const [imageUrl, setImageUrl] = useState(null);
  const [text, setText] = useState('');
  const [fontSize, setFontSize] = useState(16);
  const [textBaseline, setTextBaseline] = useState('bottom');
  const [renderProgress, setRenderProgress] = useState(0);

  const imageRef = useRef(null);

  const renderGif = () => {
    setRenderProgress(0);
    setProcessing(true);

    // Image height
    const { current } = imageRef;

    gifshot.createGIF(
      {
        ...captureOptions,
        text,
        textBaseline,
        fontSize: `${fontSize * (data.savedContexts[0].height / 230)}px`,
        frameDuration: data.editParams.frameDuration,
        progressCallback: setRenderProgress,
        gifWidth: data.savedContexts[0].width,
        gifHeight: data.savedContexts[0].height,
        savedRenderingContexts: data.savedContexts,
      },
      obj => {
        setProcessing(false);
        if (!obj.error) {
          setTextResult(obj.image);
          setImageUrl(data.textResultUrl);
          setTextBase64(obj.image);
        }
      }
    );
  };

  useEffect(() => {
    renderGif();
  }, []);

  return (
    <Page
      renderToolbar={() => (
        <Toolbar>
          <div className="left">
            <BackButton>Back</BackButton>
          </div>
          <div className="center">Add text</div>
        </Toolbar>
      )}
    >
      <PageWrap>
        <Wizard step="3" />
        <PreviewWrap
          style={{ paddingBottom: `${Math.min(0.8, 1 / aspect) * 100}%` }}
        >
          <PreviewContentWrap>
            {!processing && (
              <>
                <HeadImage src={imageUrl} ref={imageRef} />
                <GifText style={{ fontSize, [textBaseline]: 0 }}>
                  {text}
                </GifText>
              </>
            )}
            {processing && (
              <LoadingWrap>
                <ImageProgressCircular indeterminate />
              </LoadingWrap>
            )}
          </PreviewContentWrap>
        </PreviewWrap>
        <ProgressBar
          value={renderProgress * 100}
          style={{ opacity: processing ? 1 : 0 }}
        />
        <SettingsWrap
          style={{
            minHeight: `200px`,
          }}
        >
          <Input
            value={text}
            onChange={e => {
              setText(e.target.value);
            }}
            placeholder="Enter gif text"
            type="text"
            modifier="underbar"
            float
            style={{
              marginTop: '15px',
              width: '100%'
            }}
          />
          <Row style={{ marginTop: '25px' }}>
            <label>Font size</label>
          </Row>
          <Row>
            <Col
              width="40px"
              style={{ textAlign: 'center', lineHeight: '31px' }}
            >
              <Icon icon="md-format-size" size={{ default: 18 }} />
            </Col>
            <Col>
              <Range
                disabled={processing}
                value={fontSize}
                onChange={e => {
                  setFontSize(parseInt(e.target.value));
                }}
                min="1"
                max="150"
                style={{ width: '100%' }}
                id="fontSizeRange"
              />
            </Col>
            <Col
              width="40px"
              style={{ textAlign: 'center', lineHeight: '31px' }}
            >
              <Icon icon="md-format-size" size={{ default: 25 }} />
            </Col>
          </Row>

          <Row style={{ marginTop: '25px' }}>
            <label>Text position</label>
          </Row>
          <Row style={{ marginTop: '15px' }}>
            <Col>
              <Radio
                disabled={processing}
                checked={textBaseline === 'top'}
                name="position"
                onChange={() => setTextBaseline('top')}
                inputId="position-top"
              />
              <label htmlFor="position-top">
                <Icon
                  icon="md-format-valign-top"
                  size={{ default: 18 }}
                  style={{ marginRight: 10 }}
                />
                Top
              </label>
            </Col>
            <Col>
              <Radio
                disabled={processing}
                checked={textBaseline === 'bottom'}
                name="position"
                onChange={() => setTextBaseline('bottom')}
                inputId="position-bottom"
              />
              <label htmlFor="position-bottom">
                <Icon
                  icon="md-format-valign-bottom"
                  size={{ default: 18 }}
                  style={{ marginRight: 10 }}
                />
                Bottom
              </label>
            </Col>
          </Row>
        </SettingsWrap>
        <ButtonsWrap>
          <ActionButton ripple onClick={renderGif} disabled={processing}>
            Apply
          </ActionButton>

          <ActionButton
            ripple
            onClick={() => {
              navigator.pushPage({
                path: '/result',
              });
            }}
            disabled={processing}
          >
            Continue
          </ActionButton>
        </ButtonsWrap>
      </PageWrap>
    </Page>
  );
};

export default AddTextPage;
