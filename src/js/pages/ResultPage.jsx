import styled from 'styled-components';
import React, { useRef } from 'react';
import { Plugins, FilesystemDirectory } from '@capacitor/core';
import { SocialSharing } from '@ionic-native/social-sharing';
const { Filesystem, Share, FileSharer } = Plugins;

import Wizard from 'components/Wizard';

import shareIcon from 'static/images/share.svg';

import { Toolbar, BackButton } from 'react-onsenui';
import { Page, H1, H2, H4, P, TaglineText } from 'components/BasicComponents';
import Button from 'components/Button';

import { data } from 'utils/memoryStore';
import gifshot from 'gifshot';

const ResultPageContainer = styled(Page)`
  text-align: center;
`;

const PageWrap = styled.section`
  height: 100%;
  display: flex;
  flex-direction: column;
`;

const HeadImage = styled.img`
  width: 100%;
  height: 40%;
  object-fit: contain;
`;

const ButtonSection = styled.section`
  margin: 0 35px 20px 35px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  flex-grow: 1;
`;

const StartPage = ({ navigator }) => {
  const inputRef = useRef(null);

  const shareGif = async () => {
    try {
      await FileSharer.share({
        filename: 'gifer.gif',
        base64Data: data.textResultBase64.split('base64,')[1],
        contentType: 'image/gif',
      });
    } catch (e) {
      console.error(e);
    }
  };

  return (
    <ResultPageContainer
      renderToolbar={() => (
        <Toolbar>
          <div className="left">
            <BackButton>Back</BackButton>
          </div>
          <div className="center">GIF Ready!</div>
        </Toolbar>
      )}
    >
      <PageWrap>
        <HeadImage src={data.textResultUrl} />
        <Wizard step="4" />
        <ButtonSection>
          <H1 style={{ marginTop: 10 }}>Gif complete 🎉</H1>
          <TaglineText>
            Like what you made?
          </TaglineText>
          <Button onClick={shareGif} icon={shareIcon}>
            Share
          </Button>
          <P>Or make a new one</P>
          <Button onClick={() => {
            navigator.resetPage({
              path: `/start?key=${Math.random()}`
            });
          }}>
            New GIF
          </Button>
        </ButtonSection>
      </PageWrap>
    </ResultPageContainer>
  );
};

export default StartPage;
