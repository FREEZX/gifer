import styled from 'styled-components';
import React, { useRef } from 'react';

import tvIcon from 'static/images/tv_icon.svg';
import recordIcon from 'static/images/camera.svg';
import pickIcon from 'static/images/media.svg';

import { Page, H1, H2, H4, P, TaglineText } from 'components/BasicComponents';
import Button from 'components/Button';
import Wizard from 'components/Wizard';

import { setVideoFile } from 'utils/memoryStore';
import gifshot from 'gifshot';

const StartPageContainer = styled(Page)`
  text-align: center;
`;

const HeadImage = styled.img`
  width: 100%;
  height: 40%;
  object-fit: cover;
`;

const PageWrap = styled.section`
  height: 100%;
  display: flex;
  flex-direction: column;
`;

const ButtonSection = styled.section`
  margin: 0 35px 20px 35px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  flex-grow: 1;
`;

const captureOptions = {
  duration: 10,
};

const StartPage = ({ navigator }) => {
  const inputRef = useRef(null);

  const loadVideo = isCapture => () => {
    const { current: currentRef } = inputRef;
    if (isCapture) {
      currentRef.setAttribute('capture', true);
    } else {
      currentRef.removeAttribute('capture');
    }
    currentRef.click();
  };

  const inputChanged = e => {
    const { current: currentRef } = inputRef;
    if (currentRef.files[0]) {
      setVideoFile(currentRef.files[0]);
      navigator.pushPage({
        path: '/edit',
      });
    }
  };

  return (
    <StartPageContainer>
      <PageWrap>
        <HeadImage src={tvIcon} />
        <Wizard step="1" />
        <ButtonSection>
          <H1 style={{marginTop: 0}}>Gifer</H1>
          <TaglineText>
            Get ready to create something{' '}
            <span style={{ color: '#FF0000' }}>A</span>
            <span style={{ color: '#FF9900' }}>M</span>
            <span style={{ color: '#E3E300' }}>A</span>
            <span style={{ color: '#28FF00' }}>Z</span>
            <span style={{ color: '#00EDFF' }}>I</span>
            <span style={{ color: '#4300FF' }}>N</span>
            <span style={{ color: '#FF00FF' }}>G</span>
            🚀
          </TaglineText>
          <input
            hidden
            type="file"
            accept="video/*"
            ref={inputRef}
            onChange={inputChanged}
          />
          <Button ripple icon={recordIcon} onClick={loadVideo(true)}>
            Record video
          </Button>
          <Button
            ripple
            icon={pickIcon}
            onClick={loadVideo(false)}
            style={{ marginTop: '25px' }}
          >
            Pick video
          </Button>
        </ButtonSection>
      </PageWrap>
    </StartPageContainer>
  );
};

export default StartPage;
