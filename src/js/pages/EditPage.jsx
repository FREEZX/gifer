import styled from 'styled-components';
import React, { useRef, useEffect, useState } from 'react';

import gifIcon from 'static/images/gif.svg';
import refreshIcon from 'static/images/refresh.svg';

import {
  Row,
  Col,
  Icon,
  ProgressBar,
  BackButton,
  Toolbar,
  ProgressCircular,
} from 'react-onsenui';
import {
  Page,
  H1,
  H2,
  H4,
  P,
  Input,
  Range,
  WarningLabel,
  LoadingWrap,
  ImageProgressCircular,
  PreviewWrap,
  PreviewContentWrap,
  ButtonsWrap,
  ActionButton,
} from 'components/BasicComponents';
import Button from 'components/Button';
import Wizard from 'components/Wizard';

import {
  data,
  setResult,
  setSavedContexts,
  setEditParams,
} from 'utils/memoryStore';
import gifshot from 'gifshot';

const HeadImage = styled.img`
  width: 100%;
  max-height: 100%;
  object-fit: contain;
`;

const SubLabel = styled.label`
  color: ${props => props.theme.grayed};
  font-size: 11px;
`;

const FilesizeWarnLabel = styled(WarningLabel)`
  font-size: 11px;
`;

const PageWrap = styled.section`
  height: 100%;
  display: flex;
  flex-direction: column;
`;

const SettingsWrap = styled.div`
  margin: 0;
  padding: 0 20px;
  display: block;
  justify-content: flex-start;
  flex-grow: 1;
  overflow-y: scroll;
`;

let captureOptions = {
  saveRenderingContexts: true,
};

const EditPage = ({ navigator }) => {
  const [videoElement, setVideoElement] = useState(null);
  const [aspect, setAspect] = useState(1);
  const [processing, setProcessing] = useState(false);
  const [videoLength, setVideoLength] = useState(1);

  const [minQuality, setMinQuality] = useState(64);
  const [maxQuality, setMaxQuality] = useState(100);
  const [quality, setQuality] = useState(100);

  const [imageUrl, setImageUrl] = useState(null);
  const [interval, setFrameInterval] = useState(100);
  const [startOffset, setStartOffset] = useState(0);
  const [frameDuration, setFrameDuration] = useState(1);
  const [numFrames, setNumFrames] = useState(10);
  const [renderProgress, setRenderProgress] = useState(0);

  const imageRef = useRef(null);

  const renderGif = () => {
    setProcessing(true);

    setRenderProgress(0);
    const gifParams = {
      ...captureOptions,
      video: [data.videoFile],
      interval: interval * 0.001,
      numFrames,
      frameDuration,
      offset: startOffset / 1000.0,
      progressCallback: setRenderProgress,
      gifWidth: quality,
      gifHeight: Math.floor(quality * (1 / aspect)),
    };

    gifshot.createGIF(gifParams, obj => {
      setProcessing(false);
      if (!obj.error) {
        setResult(obj.image);
        setImageUrl(data.resultUrl);
        setSavedContexts(obj.savedRenderingContexts);
        setEditParams(gifParams);
      }
    });
  };

  useEffect(() => {
    const videoElement = document.createElement('video');
    videoElement.setAttribute('src', data.videoUrl);
    document.body.appendChild(videoElement);
    setVideoElement(videoElement);
    videoElement.addEventListener('loadedmetadata', () => {
      setAspect(videoElement.videoWidth / videoElement.videoHeight);
      setMinQuality(Math.min(videoElement.videoWidth * 0.1, 64));
      setMaxQuality(videoElement.videoWidth);
      setQuality(videoElement.videoWidth * 0.1);
      setVideoLength(videoElement.duration);
    });

    window.scrollTo(0, 0);
    return () => {
      document.body.removeChild(videoElement);
    };
  }, []);

  useEffect(() => {
    if (videoElement != null) {
      renderGif();
    }
  }, [aspect]);

  return (
    <Page
      renderToolbar={() => (
        <Toolbar>
          <div className="left">
            <BackButton>Back</BackButton>
          </div>
          <div className="center">Clip it</div>
        </Toolbar>
      )}
    >
      <PageWrap>
        <Wizard step="2" />
        <PreviewWrap
          style={{ paddingBottom: `${Math.min(0.8, 1 / aspect) * 100}%` }}
        >
          <PreviewContentWrap>
            {!processing && <HeadImage src={imageUrl} ref={imageRef} />}
            {processing && (
              <LoadingWrap>
                <ImageProgressCircular indeterminate />
              </LoadingWrap>
            )}
          </PreviewContentWrap>
        </PreviewWrap>
        <ProgressBar
          value={renderProgress * 100}
          style={{ opacity: processing ? 1 : 0 }}
        />
        <SettingsWrap
          style={{
            minHeight: `calc(100% - 178px - 100wh * ${aspect})`,
            maxHeight: `calc(100% - 178px - 100wh * ${aspect})`,
          }}
        >
          <Row style={{ marginTop: '25px' }}>
            <label>
              Quality: {quality}x{Math.floor((quality * 1) / aspect)}px
            </label>
          </Row>
          <Row>
            <SubLabel>Higher = slower, bigger file</SubLabel>
          </Row>
          <Row>
            <Col
              width="40px"
              style={{ textAlign: 'center', lineHeight: '31px' }}
            >
              <Icon icon="md-photo-size-select-small" size={{ default: 18 }} />
            </Col>
            <Col>
              <Range
                disabled={processing}
                value={quality}
                onChange={e => {
                  setQuality(parseInt(e.target.value));
                }}
                min={minQuality}
                max={maxQuality}
                step="1"
                style={{ width: '100%' }}
              />
            </Col>
            <Col
              width="40px"
              style={{ textAlign: 'center', lineHeight: '31px' }}
            >
              <Icon icon="md-photo-size-select-small" size={{ default: 25 }} />
            </Col>
          </Row>

          <Row style={{ marginTop: '25px' }}>
            <label>Frames to capture: {numFrames}</label>
          </Row>
          <Row>
            <SubLabel>Higher = slower, bigger file</SubLabel>
          </Row>
          <Row>
            <Col
              width="40px"
              style={{ textAlign: 'center', lineHeight: '31px' }}
            >
              <Icon icon="md-image" size={{ default: 18 }} />
            </Col>
            <Col>
              <Range
                disabled={processing}
                value={numFrames}
                onChange={e => {
                  setNumFrames(parseInt(e.target.value));
                }}
                min="1"
                max={Math.min(
                  (videoLength - startOffset / 1000) / (interval * 0.001),
                  150
                )}
                style={{ width: '100%' }}
              />
            </Col>
            <Col
              width="40px"
              style={{ textAlign: 'center', lineHeight: '31px' }}
            >
              <Icon icon="md-collection-image" size={{ default: 18 }} />
            </Col>
          </Row>

          <Row style={{ marginTop: '25px' }}>
            <label>Start offset: {startOffset}ms</label>
          </Row>
          <Row>
            <Col
              width="40px"
              style={{ textAlign: 'center', lineHeight: '31px' }}
            >
              <Icon icon="md-timer" size={{ default: 18 }} />
            </Col>
            <Col>
              <Range
                disabled={processing}
                value={startOffset}
                onChange={e => {
                  setStartOffset(parseInt(e.target.value));
                  setNumFrames(
                    Math.min(
                      Math.ceil(
                        Math.min(
                          (videoLength - startOffset / 1000) /
                            (interval * 0.001),
                          150
                        )
                      ),
                      numFrames
                    )
                  );
                }}
                min="0"
                max={videoLength * 1000 - frameDuration * numFrames}
                step="1"
                style={{ width: '100%' }}
              />
            </Col>
            <Col
              width="40px"
              style={{ textAlign: 'center', lineHeight: '31px' }}
            >
              <Icon icon="md-timer" size={{ default: 25 }} />
            </Col>
          </Row>

          <Row style={{ marginTop: '25px' }}>
            <label>Frame interval: {interval}ms</label>
          </Row>
          <Row>
            <Col
              width="40px"
              style={{ textAlign: 'center', lineHeight: '31px' }}
            >
              <Icon icon="md-timer" size={{ default: 18 }} />
            </Col>
            <Col>
              <Range
                disabled={processing}
                value={interval}
                onChange={e => {
                  setFrameInterval(parseInt(e.target.value));
                }}
                min="10"
                max="3000"
                step="10"
                style={{ width: '100%' }}
              />
            </Col>
            <Col
              width="40px"
              style={{ textAlign: 'center', lineHeight: '31px' }}
            >
              <Icon icon="md-timer" size={{ default: 25 }} />
            </Col>
          </Row>

          <Row style={{ marginTop: '25px' }}>
            <label>Frame duration: {frameDuration * 100}ms</label>
          </Row>
          <Row>
            <Col
              width="40px"
              style={{ textAlign: 'center', lineHeight: '31px' }}
            >
              <Icon icon="md-timer" size={{ default: 18 }} />
            </Col>
            <Col>
              <Range
                disabled={processing}
                value={frameDuration}
                onChange={e => {
                  setFrameDuration(parseInt(e.target.value));
                }}
                min="1"
                max="30"
                step="1"
                style={{ width: '100%' }}
              />
            </Col>
            <Col
              width="40px"
              style={{ textAlign: 'center', lineHeight: '31px' }}
            >
              <Icon icon="md-timer" size={{ default: 25 }} />
            </Col>
          </Row>
          {data.resultBlob && data.resultBlob.size > 5000000 && (
            <FilesizeWarnLabel>
              The file size exceeds 5MB. For optimal experience, try reducing
              it.
            </FilesizeWarnLabel>
          )}
        </SettingsWrap>
        <ButtonsWrap>
          <ActionButton ripple onClick={renderGif} disabled={processing}>
            Apply
          </ActionButton>
          <ActionButton
            ripple
            onClick={() => {
              navigator.pushPage({
                path: '/text',
              });
            }}
            disabled={processing}
          >
            Continue
          </ActionButton>
        </ButtonsWrap>
      </PageWrap>
    </Page>
  );
};

export default EditPage;
