const path = require('path');

module.exports = {
	"env": {
		"browser": true,
		"es6": true
	},
	"extends": [
		"eslint:recommended",
		"prettier",
		"airbnb"
	],
	"parser": "babel-eslint",
	"parserOptions": {
		"sourceType": "module"
	},
	"rules": {
		"no-console": [
			"warn"
		],
		"indent": [
			"error",
			"tab",
			{ "SwitchCase": 1 }
		],
		"linebreak-style": [
			"error",
			"unix"
		],
		"quotes": [
			"error",
			"single"
		],
		"semi": [
			"error",
			"always"
		]
	},
  "settings": {
    "import/resolver": {
      "alias": {
        "map": [
          ["pages", path.resolve(__dirname, 'pages')],
          ["components", path.resolve(__dirname, 'components')],
          ["constants", path.resolve(__dirname, 'constants')],
          ["actions", path.resolve(__dirname, 'actions')],
          ["selectors", path.resolve(__dirname, 'selectors')],
          ["utils", path.resolve(__dirname, 'utils')],
          ["static", path.resolve(__dirname, 'static')]
        ],
        "extensions": [".js", ".jsx", ".json"]
      }
    }
  }
};
