const path = require('path');
const webpack = require('webpack');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	entry: './src/js/index.jsx',
	output: {
		filename: '[name]-[hash].js',
		chunkFilename: '[name]-[hash]-chunk.js',
		path: path.resolve(__dirname, 'www')
	},
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env']
					}
				}
			},
			{ test: /\.css$/, loader: 'style-loader!css-loader' },
			{ test: /\.png$/, loader: 'file-loader' },
			{ test: /\.ttf$/, loader: 'file-loader' },
			{ test: /\.jpg$/, loader: 'file-loader' },
			{ test: /\.woff$/, loader: 'file-loader' },
			{ test: /\.woff2$/, loader: 'file-loader' },
			{ test: /\.eot$/, loader: 'file-loader' },
			{ test: /\.svg$/, loader: 'file-loader' },
			{ test: /\.otf$/, loader: 'file-loader' },
			{ test: /\.gif$/, loader: 'file-loader' },
			{ test: /\.docx$/, loader: 'file-loader' },
			{
				test: /\.scss$/,
				use: [
					{
						loader: 'style-loader'
					},
					{
						loader: 'css-loader'
					},
					{
						loader: 'sass-loader'
					}
				]
			}
		]
	},
	plugins: [
		new CleanWebpackPlugin(),
		new HtmlWebpackPlugin({
			filename: 'index.html',
			template: 'src/index.html'
		}),
		// new webpack.ProvidePlugin({
		// 	$: 'jquery',
		// 	jQuery: 'jquery',
		// 	Popper: ['popper.js', 'default']
		// })
	],
  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      components: path.resolve(__dirname, 'src/js/components/'),
      pages: path.resolve(__dirname, 'src/js/pages/'),
      utils: path.resolve(__dirname, 'src/js/utils/'),
      static: path.resolve(__dirname, 'src/static/'),
    }
  },
  node: {
		fs: 'empty',
		net: 'empty',
		tls: 'empty'
	}
};
